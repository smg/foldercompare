﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace foldercompare
{
    class Program
    {
        const string outputFilename = "diff.txt";
        static void Main(string[] args)
        {
            string dir1, dir2, mask = "*", output = outputFilename;
            if (args.Length > 1)
            {
                dir1 = args[0];
                dir2 = args[1];
                if (args.Length > 2)
                {
                    mask = args[2];
                }
            }
            else
            {
                PrintHelp();
                return;
            }

            var fc = new FolderComparer();
            var result = fc.Compare(dir1, dir2, mask);

            foreach (var str in fc.GetReport())
            {
                Console.WriteLine(str);
            }
            if (args.Length > 3)
            {
                output = args[3];
            }
            Console.Read();
            WriteReport(output, fc.GetReport());
        }

        static void WriteReport(string filename, IEnumerable<string> text)
        {
            File.AppendAllLines(filename, text, Encoding.Unicode);
        }

        static void PrintHelp()
        {
            Console.WriteLine("Compares two folders by files' names (with subfolders)");
            Console.WriteLine("Usage:");
            Console.WriteLine("<first directory> <second directory> [<mask>] [<output filename>]");
            Console.WriteLine("<mask>\tsearch pattern in DirectoryInfo.GetFiles method");
            Console.WriteLine("<output filename>\tfile to append result to, default is " + outputFilename);
            Console.WriteLine();
        }
    }
}
