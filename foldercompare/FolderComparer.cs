﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace foldercompare
{
    public class FolderComparer
    {
        public string FirstDirectory { get; private set; }
        public string SecondDirectory { get; private set; }
        public string Mask { get; private set; }
        public IEnumerable<string> FilesNotInFirstFolder { get; private set; }
        public IEnumerable<string> FilesNotInSecondFolder { get; private set; }
        public int MismatchFiles { get; private set; }

        public int Compare(string firstDirectory, string secondDirectory, string mask = "*")
        {
            FirstDirectory = firstDirectory.Trim('"');
            SecondDirectory = secondDirectory.Trim('"');
            if (mask == "")
                mask = "*";
            Mask = mask;

            var firstNames = NamesOfFiles(FilesInDirectory(firstDirectory, mask));
            var secondNames = NamesOfFiles(FilesInDirectory(secondDirectory, mask));

            FilesNotInSecondFolder = firstNames.Except(secondNames);
            FilesNotInFirstFolder = secondNames.Except(firstNames);
            MismatchFiles = FilesNotInFirstFolder.Count() + FilesNotInSecondFolder.Count();
            return MismatchFiles;
        }

        public IEnumerable<string> GetReport()
        {
            List<string> report = new List<string>(5 + MismatchFiles);
            report.Add(DateTime.Now.ToString());

            if (FilesNotInFirstFolder == null)
            {
                report.Add("Not compared yet.");
                return report;
            }

            report.Add(string.Format("Difference between {0} and {1}, mask \"{2}\", number of files: {3}", FirstDirectory, SecondDirectory, Mask, MismatchFiles));
            report.Add(string.Format("Files in {0}", FirstDirectory));
            report.AddRange(FilesNotInSecondFolder);
            report.Add(string.Format("Files in {0}", SecondDirectory));
            report.AddRange(FilesNotInFirstFolder);
            report.Add("");

            return report;
        }

        static List<FileInfo> FilesInDirectory(string dir, string mask)
        {
            if (Directory.Exists(dir))
                return new DirectoryInfo(dir).GetFiles(mask, SearchOption.AllDirectories).ToList();
            else
                return new List<FileInfo>();
        }

        static IEnumerable<string> NamesOfFiles(List<FileInfo> files)
        {
            return files.Select<FileInfo, string>(file => file.Name.ToLower());
        }
    }
}
