﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.IO;
using System.Windows.Media.Imaging;

namespace gui
{
    public class PathToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string path = new TestExtensionConverter().Convert(value, targetType, parameter, culture) as string;
            if (path != null)
            {
                BitmapImage image = new BitmapImage() { DecodePixelWidth = 100, DecodePixelHeight = 75 };
                using (FileStream stream = File.OpenRead(path))
                {
                    image.BeginInit();
                    image.StreamSource = stream;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.EndInit();
                }
                return image;
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
