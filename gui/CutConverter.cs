﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace gui
{
    public class CutConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                var str = value as string;
                if (str.LastIndexOf('\\') != -1)
                    return str.Substring(str.LastIndexOf('\\') + 1);
                return value;
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
