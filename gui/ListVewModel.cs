﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace gui
{
    public class ListVewModel : ViewModelBase
    {

        private int _columns;

        private ICommand _delSel;

        private double _width;

        public ListVewModel(IEnumerable<string> paths)
        {
            Images = new ObservableCollection<ImageViewModel>();
            foreach (var path in paths)
            {
                var img = new ImageViewModel(path);
                Images.Add(img);
                img.Removed += (s, e) => { Images.Remove(s as ImageViewModel); };
            }

        }

        public int ColumnsCount
        {
            get
            {
                return _columns;
            }
            set
            {
                if (_columns != value)
                {
                    _columns = value;
                    OnPropertyChanged(() => ColumnsCount);
                }
            }
        }

        public ICommand DeleteSelected
        {
            get
            {
                return _delSel
                   ?? (_delSel = new RelayCommand(() =>
                        {
                            foreach (var img in Images.Where(i => i.IsSelected).ToList())
                            {
                                img.DeleteCommand.Execute(null);
                            }
                        }));
            }
        }

        public ObservableCollection<ImageViewModel> Images
        {
            get;
            set;
        }
        public double Width
        {
            get
            {
                return _width;
            }
            set
            {
                if (_width != value)
                {
                    _width = value;
                    ColumnsCount = (int)(Width / 120);
                    OnPropertyChanged(() => Width);
                }
            }
        }
    }
}
