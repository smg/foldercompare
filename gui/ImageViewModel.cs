﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.VisualBasic.FileIO;

namespace gui
{
    public class ImageViewModel : ViewModelBase
    {
        string path;
        private ICommand _delCommand;
        private bool _isSelected;
        private ICommand _open;
        public event EventHandler Removed;
        public string Name
        {
            get
            {
                if (path.LastIndexOf('\\') != -1)
                    return path.Substring(path.LastIndexOf('\\') + 1);
                else
                    return path;
            }
        }
        public bool IsSelected
        {
            get
            {
                return _isSelected;
            }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(() => IsSelected);
                }
            }
        }

        public BitmapImage Bitmap
        {
            get
            {
                BitmapImage image = new BitmapImage();
                var imgpath = FilterNonImages(path);
                using (FileStream stream = File.OpenRead(imgpath))
                {
                    image.BeginInit();
                    image.StreamSource = stream;
                    image.DecodePixelWidth = 100;
                    image.DecodePixelHeight = 75;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.EndInit();
                }
                return image;
            }
        }
        public ICommand DeleteCommand
        {
            get
            {
                return _delCommand
                   ?? (_delCommand = new RelayCommand(() =>
                        {
                            try
                            {
                                FileSystem.DeleteFile(path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("deleting {0}: {1}", path, e.Message);
                            }
                            finally
                            {
                                if (!File.Exists(path))
                                {
                                    OnRemoved();
                                }
                            }
                        }));
            }
        }
        public ICommand OpenCommand
        {
            get
            {
                return _open
                   ?? (_open = new RelayCommand(() =>
                        {
                            if (File.Exists(path))
                                System.Diagnostics.Process.Start(path);
                        }));
            }
        }

        protected virtual void OnRemoved()
        {
            var h = Removed;
            if (h != null)
            {
                h(this, EventArgs.Empty);
            }
        }

        public ImageViewModel(string fullpath)
        {
            path = fullpath;

        }

        static string FilterNonImages(string path)
        {
            if (path.EndsWith("jpg", StringComparison.InvariantCultureIgnoreCase) ||
                path.EndsWith("jpeg", StringComparison.InvariantCultureIgnoreCase) ||
                path.EndsWith("png", StringComparison.InvariantCultureIgnoreCase))
                return path;

            return Path.Combine(Directory.GetCurrentDirectory(), "dummy.png");
        }
    }
}
