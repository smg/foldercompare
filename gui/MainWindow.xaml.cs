﻿using System.Threading;
using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Linq;
using gui.Properties;

namespace gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string _firstDir, _secondDir, _mask, _outputFile;
        Thickness _tbBorderWidth;
        System.Windows.Forms.OpenFileDialog fileDialog = new System.Windows.Forms.OpenFileDialog();
        System.Windows.Forms.FolderBrowserDialog folderDialog = new System.Windows.Forms.FolderBrowserDialog();
        public MainWindow()
        {
            InitializeComponent();

            outputTextBox.Text = Settings.Default.outputFile;
            firstFolderTextBox.Text = Settings.Default.firstFolder;
            secondFolderTextBox.Text = Settings.Default.seconfFolder;
            maskTextBox.Text = Settings.Default.mask;
        }

        #region Buttons

        private void ChooseFile_Click(object sender, RoutedEventArgs e)
        {
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outputTextBox.Text = fileDialog.FileName;
            }
        }

        private void ChooseFolder_Click(object sender, RoutedEventArgs e)
        {
            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Button b = sender as Button;
                if (b == firstFolderChooser)
                {
                    firstFolderTextBox.Text = folderDialog.SelectedPath;
                }
                else
                {
                    secondFolderTextBox.Text = folderDialog.SelectedPath;
                }
            }
        }
        private void Compare_Click(object sender, RoutedEventArgs e)
        {
            progress.Visibility = System.Windows.Visibility.Visible;
            compareButton.IsEnabled = false;

            DoComparsion();

            progress.Visibility = System.Windows.Visibility.Collapsed;
            compareButton.IsEnabled = true;

            //BackgroundWorker bw = new BackgroundWorker();
            //bw.WorkerReportsProgress = true;
            //bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            //bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            //bw.RunWorkerAsync();
        }

        #endregion

        #region Comapre

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            progress.Visibility = System.Windows.Visibility.Visible;
            compareButton.IsEnabled = false;

        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progress.Visibility = System.Windows.Visibility.Collapsed;
            compareButton.IsEnabled = true;
        }

        private void textBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                DoComparsion();
            }
        }
        private void DoComparsion()
        {
            var fc = new foldercompare.FolderComparer();
            fc.Compare(_firstDir, _secondDir, _mask);

            var left = new ListVewModel(fc.FilesNotInSecondFolder.Select(f => fc.FirstDirectory + @"\" + f));
            var right = new ListVewModel(fc.FilesNotInFirstFolder.Select(f => fc.SecondDirectory + @"\" + f));
            leftImages.DataContext = left;
            rightImages.DataContext = right;

            if (output.IsChecked.HasValue && output.IsChecked.Value)
                System.IO.File.AppendAllLines(_outputFile, fc.GetReport(), Encoding.Unicode);
        }

        #endregion

        #region Text changing

        private void firstFolderTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _firstDir = firstFolderTextBox.Text;

        }

        private void maskTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _mask = maskTextBox.Text;
        }

        private void outputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _outputFile = outputTextBox.Text;
        }

        private void secondFolderTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _secondDir = secondFolderTextBox.Text;
        }
        #endregion

        #region Drag n Drop

        private void folderTextBox_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.UnicodeText, true) ||
                e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void folderTextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                _tbBorderWidth = tb.BorderThickness;
                tb.BorderThickness = new Thickness(3);
            }
            e.Handled = true;
        }

        private void folderTextBox_PreviewDragLeave(object sender, DragEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                tb.BorderThickness = _tbBorderWidth;
            }
        }

        private void folderTextBox_PreviewDrop(object sender, DragEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb != null)
            {
                if (e.Data.GetDataPresent(DataFormats.UnicodeText, true))
                {
                    tb.Text = (string)e.Data.GetData(DataFormats.UnicodeText, true);
                }
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    var paths = (string[])e.Data.GetData(DataFormats.FileDrop);
                    tb.Text = paths[0];
                }
                tb.BorderThickness = _tbBorderWidth;
            }
            e.Handled = true;
        }

        #endregion
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Settings.Default.firstFolder = _firstDir;
            Settings.Default.seconfFolder = _secondDir;
            Settings.Default.mask = _mask;
            Settings.Default.outputFile = _outputFile;
            Settings.Default.Save();
        }
    }
}
