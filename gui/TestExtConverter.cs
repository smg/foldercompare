﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.IO;

namespace gui
{
    public class TestExtensionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
                object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                var str = value as string;
                if (str.EndsWith("jpg", StringComparison.InvariantCultureIgnoreCase) ||
                    str.EndsWith("jpeg", StringComparison.InvariantCultureIgnoreCase) ||
                    str.EndsWith("png", StringComparison.InvariantCultureIgnoreCase))
                    return str;
            }
            return Path.Combine(Directory.GetCurrentDirectory(), "dummy.png");
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
